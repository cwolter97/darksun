const Discord = require("discord.js");
//const logger = require('winston');
//const botSettings = require("./settings.json");
var environment = 'remote';

var botSettings;

if(typeof process.env.token === 'undefined'){
  botSettings = require('./settings.json');
  environment = 'local';
}

var twitch_client_id, bot_token, yt_api_key, prefix, interval;

if(environment == 'local'){
  twitch_client_id = botSettings.twitch_api;
  bot_token = botSettings.token;
  prefix = botSettings.prefix;
  interval = botSettings.interval * 1000;
  streamers = botSettings.streamers;
  pubg_token = botSettings.pubg_token;
  pubg_players = botSettings.pubg_players;
  spam = true;
}else{
  twitch_client_id = process.env.twitch_api;
  bot_token = process.env.token;
  prefix = process.env.prefix;
  interval = process.env.interval * 1000;
  streamers = process.env.streamers;
  pubg_token = process.env.pubg_token;
  pubg_players = process.env.pubg_players;
  spam = false;
}

//Checking Online Streamer Status
const streamers_array = streamers.split(" ");
var streamer_status = new Array(streamers_array.length);
var streamer_list = {listExists: true};

streamers_array.forEach(function(item){
    streamer_list[item] = false;
});

delete streamer_list['listExists'];
//console.log(streamer_list);

function checkStreamStatus(list){
  for(name in list){

    stream_name = name;
    isLive = list[name];

    console.log("checking stream status of: " + stream_name);
    //if(isLive == true){ console.log("already live"); }
    var request = require("request-promise");

    var options = { method: 'GET',
      url: `https://api.twitch.tv/kraken/streams/${stream_name}`,
      headers: {
        'Client-ID': twitch_client_id
      }
    }

    request(options, function(error, response, body){
      let jsonResponse = JSON.parse(body);

      //strim_name = stream_name;

      if(jsonResponse.stream != null){
        var rightNow = new Date();
        var x = rightNow.toISOString();
        //console.log(jsonResponse.stream);
        let embed = new Discord.RichEmbed()
            //.setAuthor(message.author.usernam)
            .setAuthor(jsonResponse.stream.channel.display_name, jsonResponse.stream.channel.logo)
            //.setDescription(jsonResponse.stream.channel.display_name + " is streaming: ")
            .setColor("#9B59B6")
            .setDescription("**Playing**: " + jsonResponse.stream.game)
            .setTitle(jsonResponse.stream.channel.status)
            .setURL(jsonResponse.stream.channel.url)
            .setImage(jsonResponse.stream.preview.medium)
            .setTimestamp(x)

            if(spam){
              let channel = channel_spam;
            } else {
              let channel = channel_strims;
            }

            if(jsonResponse.stream != null){ isLive = list[jsonResponse.stream.channel.name] };

            if(!isLive){
              channel_strims.send("Now Live: " + jsonResponse.stream.channel.display_name + "! @here");
              channel_strims.send(embed);
              list[jsonResponse.stream.channel.name] = true;
              console.log(`${jsonResponse.stream.channel.name}.online = ${list[jsonResponse.stream.channel.name]}`);
              //console.log(`online: `);
            }
        } else {
          strim_name = jsonResponse._links.channel.substring(jsonResponse._links.channel.lastIndexOf('/')+1)
          streamer_list[strim_name] = false;
          console.log(`${strim_name}.online = ${list[strim_name]}`);
          //console.log(`online: ${list[strim_name]}`);
        }
    })
  }
}
//End Of Checking Streamer Status

//Checking For PUBG Chiken Diner
var lastCheck = new Date();

pubg_players_array = pubg_players.split(",");
pubg_status_check = new Array(pubg_players_array.length);

var pubg_lastCheck = {listExists: true};

pubg_players_array.forEach(function(item){
    pubg_lastCheck[item] = lastCheck;
});

delete pubg_lastCheck['listExists'];

function newMatch(updatedTime, player){
  console.log(updatedTime);
 	update = new Date(updatedTime);
	timediff = (update - pubg_lastCheck[player])/1e3/60;
	// console.log(`${timediff} minutes since last profile update`);
	// console.log(`Current Profile Update: ${update}`);
	// console.log(`Last Profile Update: ${lastCheck} (Could be time bot was started)`);

	if(timediff > 0){
		console.log(`last check updated for ${player}!`);
		pubg_lastCheck[player] = update;
		return true;
  } else {
    console.log(`no new matches for ${player}!`)
    return false;
  }
}

function postChickenDinner(){
  var request = require("request-promise");

  var options = { method: 'GET',
    url: `https://api.playbattlegrounds.com/shards/pc-na/players?filter[playerNames]=${pubg_players}`,
    headers: {
      'Authorization': `Bearer ${pubg_token}`,
      'Accept': 'application/vnd.api+json'
    }
  }

  request(options, function(error, response, body){
    console.log("Making Request");
  }).then((body) => {
    console.log("Request Complete");
    let jsonResponse = JSON.parse(body);
    //console.log(jsonResponse);
    jsonResponse.data.forEach((player) => {
      console.log("FROM REQUEST TIME: " + player.attributes.updatedAt);
      if(newMatch(player.attributes.updatedAt, player.attributes.name)){
          console.log("--------NEW MATCH---------");
          lastMatch = player.relationships.matches.data[0].id;
          console.log(`Last Match ID: ${lastMatch}`);

          var matchInfoRequest = require("request-promise");
          var options = { method: 'GET',
            url: `https://api.playbattlegrounds.com/shards/pc-na/matches/${lastMatch}`,
            headers: {
              'Authorization': `Bearer ${pubg_token}`,
              'Accept': 'application/vnd.api+json'
            }
          }
          matchInfoRequest(options, function(matchError, matchResponse, matchBody){
              console.log("Making Match Info Request for " + player.attributes.name);
          }).then((matchBody) => {
              console.log("Completed Match Info Request for " + player.attributes.name);
              console.log("-----MATCH END INFO------")
              matchResponse = JSON.parse(matchBody);
              //console.log(matchResponse);
              var x = 0, y = 0;
              winners = new Array();
              matchResponse.included.forEach(function(item){
              	//console.log(item.type)
              	if(item.type == "participant"){
                      if(item.attributes.stats.winPlace == 1){
              			       //console.log(`Winner of a Chicken Dinner: ${item.attributes.stats.name}`);
                           winners[y] = item.attributes.stats.name;
                           y++;
                      }
                      x++;
                  }
              });
              if(winners.includes(player.attributes.name)){
                pubg_msg = `${player.attributes.name} just earned a chiken diner!`;
                pubg_discord_msg = `\n\`\`\`DINNER NOTIFICATION!\n${player.attributes.name} just earned a chiken diner with ${item.attributes.stats.kills} kill(s)!\`\`\``
              } else {
                pubg_msg = `${player.attributes.name} lost to ${winners}!`
                //pubg_discord_msg = `\n\`\`\`DINNER NOTIFICATION!\n${player.attributes.name} just lost to ${winners}!\`\`\``;
                pubg_discord_msg = "";
              }
              console.log(pubg_msg);
              if(pubg_discord_msg == ""){ return; }
              if(spam){
                channel_spam.send(pubg_discord_msg);
              } else {
                channel_frags.send(pubg_discord_msg);
              }
          });
      }
    });

  }).catch(console.error);;
}
//End of Checking for Chiken Diner

const bot = new Discord.Client({disableEveryone: false});
var channel_darksun, channel_spam;

bot.on("ready", async () => {
    //username = "もしもし！" //Moshi Moshi!
    //username = "ダディボット"; //Daddy-Bot
    username = "ボットちゃん"; //bot-chan

    bot.user.setUsername(username);
    channel_darksun = bot.channels.find("name", "darksun");
    channel_spam = bot.channels.find("name", "bot-fuckery");
    channel_frags = bot.channels.find("name", "frags");
    channel_strims = bot.channels.find("name", "strims");
    //x.send(`I am now online! Type ${prefix}help for help!`)
});

bot.on("message", async message => {

    if(message.author.bot) return;
    if(message.channel.type === "dm") return;

    let messageArray = message.content.split(" ");
    let command = messageArray[0];

    if(!command.startsWith(prefix)) return;

    //returns back top video id result
    function searchYoutube(query){

    }

    switch(command) {
        // case `${prefix}ping`:
        //   message.channel.send(`${message.author.toString()}`);
        //   break;
        case `${prefix}pyong`:
            message.channel.send(`${prefix}yang`);
            break;

        case `${prefix}help`:
              var minuteInterval = (interval/1000)/60;
              var help_msg = "";
              help_msg += "\n```"
              help_msg += `\n${prefix}status <channelname> - Returns "live" status of channel`
              help_msg += `\n\nThis bot is currently checking the online status of:`

              streamers_array.forEach(function(item){
                help_msg += '\n-';
                help_msg += item;
              });

              help_msg += `\nevery: ${minuteInterval} minute(s).`;
              help_msg += "\n```"
              message.author.createDM()
                .then((DMChannel) => {
                  DMChannel.send(help_msg);
                })
            break;

        case `${prefix}status`:
            if(messageArray.length != 2){ message.channel.send("`useage: !status <channelname>`"); return;}

            let msg = await message.channel.send("Checking Status...");

            var request = require("request");
            var channel_name = messageArray[1];

            var options = { method: 'GET',
              url: `https://api.twitch.tv/kraken/streams/${channel_name}`,
              headers: {
                'Client-ID': twitch_client_id
              }
            }

            request(options, function(error, response, body){
              let jsonResponse = JSON.parse(body);

              if(jsonResponse.stream != null){
                //console.log(jsonResponse.stream);
                let embed = new Discord.RichEmbed()
                    //.setAuthor(message.author.usernam)
                    .setAuthor(jsonResponse.stream.channel.display_name, jsonResponse.stream.channel.logo)
                    //.setDescription(jsonResponse.stream.channel.display_name + " is streaming: ")
                    .setColor("#9B59B6")
                    .setDescription("**Playing**: " + jsonResponse.stream.game)
                    .setTitle(jsonResponse.stream.channel.status)
                    .setURL(jsonResponse.stream.channel.url)
                    .setImage(jsonResponse.stream.preview.medium)

                message.channel.send(embed);
              }else{
                let embed = new Discord.RichEmbed()
                  .setAuthor(channel_name)
                  .setColor("#9B59B6")
                  .setDescription("This channel is offline!")
                  message.channel.send(embed);
              }
            })

            msg.delete(); //deletes "Checking Status" message

            break;
    }
});


bot.login(bot_token).then((token) => {
  //checkStreamStatus("darksunlive");
  checkStreamStatus(streamer_list);
  postChickenDinner();
  setInterval(function(){ checkStreamStatus(streamer_list); postChickenDinner(); }, interval);
}).catch(console.error);
