# darksun
A simple discord bot for my group's discord server.

Features:

!help - Lists commands

!status <twitch.tv channel name> - Returns "live" status of channel with embedded message.

Checks online status of a list of streamers (environment variable) at interval (environment variable). Then mentions @here with an embedded message containing informaiton of the stream.
